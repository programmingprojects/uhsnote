//
//  NoteView.swift
//  UHSNote
//
//  Created by Spencer Sallay on 10/11/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class NoteView: UIView {
    
    var cmds : [[Float]] = [[10.0,150.0,60.0,120.0]]
    
    override func drawRect(rect: CGRect) {
        let cmds2 : [[CGFloat]] = cmds.map{(x:[Float]) in x.map{(y:Float) in CGFloat(y)}}
        print(cmds)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, UIColor(red:1.0,green:0.0,blue:0.0,alpha:1.0).CGColor)
        CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.5)
        cmds2.forEach{x in CGContextAddRect(context, CGRectMake(x[0],x[1],x[2],x[3]))}
        //CGContextAddRect(context, CGRectMake(10.0, 150.0, 60.0, 120.0))
        CGContextFillPath(context)
    }

}
