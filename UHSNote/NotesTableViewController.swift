//
//  NotesTableViewController.swift
//  UHSNote
//
//  Created by Brett Keck on 9/1/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {

 //   var notes : [Note]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NoteStore.sharedInstance.getCount()    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NoteCell", forIndexPath: indexPath) as! NoteTableViewCell

        cell.setupCell(NoteStore.sharedInstance.getNote(indexPath.row))

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
           NoteStore.sharedInstance.deleteNote(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editNoteSegue" {
            let noteDetailVC = segue.destinationViewController as! NoteDetailViewController
            let tableCell = sender as! NoteTableViewCell
            noteDetailVC.note = tableCell.note
        }
    }
    
    @IBAction func saveNoteDetail(segue: UIStoryboardSegue) {
        let noteDetailVC = segue.sourceViewController as! NoteDetailViewController
        if let indexPath = tableView.indexPathForSelectedRow {
          // NoteStore.sharedInstance.updateNote(noteDetailVC.note, index: indexPath.row)
            NoteStore.sharedInstance.sort()
            var indexPaths = [NSIndexPath]()//tells us which is changed
            for index in 0...indexPath.row{
                indexPaths.append(NSIndexPath(forItem: index, inSection: 0))
            }
            tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        } else {
            NoteStore.sharedInstance.addNote(noteDetailVC.note)
           // let indexPath = NSIndexPath(forRow: NoteStore.sharedInstance.getCount() - 1, inSection: 0)
           // tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.reloadData()
        }
    }
}








