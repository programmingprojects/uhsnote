//
//  NoteTableViewCell.swift
//  UHSNote
//
//  Created by Brett Keck on 9/2/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    weak var note : Note!
    
    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var noteText: UILabel!
    @IBOutlet weak var noteDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(note : Note) {
        self.note = note
        noteTitle.text = note.title
        noteText.text = note.text
        noteDate.text = note.dateString
    }
}






