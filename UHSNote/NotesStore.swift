//
//  NotesStore.swift
//  UHSNote
//
//  Created by Sam Klarquist on 9/10/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//



import Foundation
//creates an instance of note store class
private let NoteStoreInstance = NoteStore()

class NoteStore{
    private var notes : [Note]!
    
    class var sharedInstance : NoteStore{
    return NoteStoreInstance
    }

    init(){
        load()
    }
    
    func getNote(index: Int) -> Note {
        return notes[index]
    }
    
    func addNote(note : Note){
     //   notes.append(note)
        notes.insert(note, atIndex: 0)
    }
    
    func deleteNote(n : Int) { notes.removeAtIndex(n) }
    
    func updateNote(note : Note, index: Int){
        notes[index] = note
    }
    
    func getCount() -> Int {
        return notes.count
    }
    func sort(){
        notes = notes.sort{
        $0.date.compare($1.date) == NSComparisonResult.OrderedDescending
        }
    }
    
    private func arciveFilePath() -> String{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let  documentsDirectory = paths.first!
        let path = (documentsDirectory as NSString).stringByAppendingPathComponent("NoteStore.plist")
        return path
    }
    
    func save() {
        NSKeyedArchiver.archiveRootObject(notes, toFile: arciveFilePath())
    }
    
    private func load(){
     let filePath = arciveFilePath()
    let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(filePath){
            notes = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as! [Note]
        }
        else{//only happens first time
            notes = []
            
            notes.append(Note(title: "Note 1", text: "0 0 0 0"))
            notes.append(Note(title: "Note 2", text: "0 0 0 0"))
            notes.append(Note(title: "Note 3", text: "0 0 0 0"))
        }
        sort()
    }
    
}