//
//  NoteDetailViewController.swift
//  UHSNote
//
//  Created by Brett Keck on 9/2/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {
    
    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var noteText: UITextView!
    @IBOutlet weak var noteView: NoteView!
    var note = Note()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        noteTitle.text = note.title
        noteText.text = note.text
        //noteView = note.view
        let temp = note.text.characters.split{$0 == " "}.map(String.init)
        noteView.cmds = temp.reduce([[0.0,0.0,0.0,0.0]], combine: {(x:[[Float]],y:String) -> [[Float]] in x.last!.count%4==0 ? x + [[(y as NSString).floatValue]] : x[0..<x.count-1] + [(x.last!+[(y as NSString).floatValue])];})
        print(noteView.cmds)
        //noteView.setNeedsDisplay()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        note.title = noteTitle.text!
        note.text = noteText.text
        //noteView.setNeedsDisplay()
        note.view = noteView
        note.view.setNeedsDisplay()
        print("cmds"); print(note.view.cmds)
        note.date = NSDate()
    }

}
