//
//  Note.swift
//  UHSNote
//
//  Created by Brett Keck on 9/1/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class Note: NSObject, NSCoding {
    var title = ""
    var text = ""
    var view = NoteView()
    var date = NSDate()
    
    var dateString : String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.stringFromDate(date)
    }
    
    override init() {
        super.init()
    }
    
    init(title: String, text: String) {
        self.title = title
        self.text = text
    }
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObjectForKey("title") as! String
        self.text = aDecoder.decodeObjectForKey("text") as! String
        self.date = aDecoder.decodeObjectForKey("date") as! NSDate
    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(title, forKey: "title")
        aCoder.encodeObject(text, forKey: "text")
        aCoder.encodeObject(date, forKey: "date")
    }
}






