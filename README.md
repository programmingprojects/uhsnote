USAGE
====

In the body of a note, groups of four integers are to be typed.  For every group of four integers, a red rectangle will be drawn onto a custom UIView in the detail controller.

The four integers are categorized like this:

```
x-coord: y-coord: width: height:
```

Edits must be saved before results are shown on the view.
Here is an example; post it into a note body, save it, and then click on the note again to view it.

```
100 50 10 10 190 50 10 10 90 100 10 10 100 110 20 20 110 120 30 30 120 130 40 40 140 130 40 40 160 120 30 30 180 110 20 20 200 100 10 10
```